# quotr

Quota manager for lustre filesystems

There's currently no way to easily manage project quotas in Lustre [1]. Until
the functionality is available I've decided to write a ~~very~~ simple app that can keep track
of project names, quotas, and paths, and generate the required commands to
perform the corresponding system updates.

[1] https://jira.whamcloud.com/browse/LU-13335
