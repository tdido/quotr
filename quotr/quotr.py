import datetime
import getpass
import grp
import os
import shlex
import shutil
import sqlite3
import subprocess
from collections import defaultdict
from importlib import metadata
from pathlib import Path
from urllib.request import pathname2url

import click
import yaml
from humanfriendly import format_size, parse_size
from packaging.version import Version
from prettytable import PrettyTable

VERSION = metadata.version("quotr")

input_date_formats = ["%Y-%m-%d", "%Y%m%d"]


def getfsquota(qid, path):
    output = subprocess.check_output(shlex.split(f"lfs quota -p {qid} {path}")).decode()
    (
        kbytes,
        fsquota,
        fsquotal,
        grace,
        files,
        fsiquota,
        fsiquotal,
        igrace,
    ) = output.split("\n")[3].split()

    kbytes = int(kbytes.rstrip("*")) * 1000
    fsquota = int(fsquota.rstrip("*")) * 1000
    fsquotal = int(fsquotal.rstrip("*")) * 1000
    files = int(files.rstrip("*"))
    fsiquota = int(fsiquota.rstrip("*"))
    fsiquotal = int(fsiquotal.rstrip("*"))

    return kbytes, fsquota, fsquotal, grace, files, fsiquota, fsiquotal, igrace


def sendmail(s, f, t, m):
    import smtplib
    from email.message import EmailMessage

    msg = EmailMessage()

    msg["Subject"] = s
    msg["From"] = f
    msg["To"] = t
    msg.set_content(m)

    s = smtplib.SMTP("mail.cnio.es")
    s.send_message(msg)
    s.quit()


def isadmin():
    return (
        True
        if set(["wheel", "sudo", "root"])
        & set([grp.getgrgid(g).gr_name for g in os.getgroups()])
        else False
    )


def checkperm(msg="ERROR: You don't have write permissions to the database."):
    if not os.access(DBFPATH, os.W_OK):
        print(f"\n{msg}\n")
        quit()


def connect(mode="rw"):
    con = sqlite3.connect(DBURI + f"?mode={mode}", uri=True)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    return con, cur


def backupdb(delete=False):
    from datetime import datetime

    ts = datetime.timestamp(datetime.now())

    backuppath = f"{BACKUPPATH}/{ts}.db"

    Path(BACKUPPATH).mkdir(parents=True, exist_ok=True)
    shutil.copyfile(f"{DBFPATH}", backuppath)
    if delete:
        os.remove(f"{DBFPATH}")
    return backuppath


def createdb():
    con, cur = connect(mode="rwc")

    cur.execute(
        "CREATE TABLE quotas(project_name, req_user, req_group, creation_date, end_date, type, deleted)"
    )
    cur.execute(
        "CREATE UNIQUE INDEX unique_project_group ON quotas(project_name, req_group);"
    )
    cur.execute("CREATE TABLE paths(quotas_id, path)")
    cur.execute("CREATE TABLE descriptions(quotas_id, description)")
    cur.execute("CREATE TABLE version(version)")
    cur.execute("INSERT INTO version VALUES (?)", (VERSION,))

    con.commit()
    con.close()


def gen_conf(file_name="quotr.conf"):
    import pkgutil

    data = pkgutil.get_data("quotr.res", "quotr.conf.example")
    with open(file_name, "wb") as ofh:
        ofh.write(data)

    print(
        "Config file generated. You can define the `QUOTR_CONFIG` env var to point",
        "to it or place it at one of the following paths:",
    )
    for p in config_paths:
        print(f"\t- {p}")


config_paths = [
    "./quotr.conf",
    f"{Path.home()}/.config/quotr/quotr.conf",
    "/etc/quotr/quotr.conf",
]

if "QUOTR_CONFIG" in os.environ and os.environ["QUOTR_CONFIG"]:
    config_paths = [os.environ["QUOTR_CONFIG"]] + config_paths

config = None
for p in config_paths:
    try:
        with open(p) as ifh:
            config = yaml.safe_load(ifh)
    except yaml.YAMLError as err:
        print(err)
    except FileNotFoundError:
        pass
    else:
        break

if not config:
    if click.confirm("Could not parse config file. Generate one?"):
        gen_conf()
    quit()


try:
    safe = config["safe_mode"]
except KeyError:
    safe = False

try:
    BASEPATH = config["paths"]["basepath"]
except KeyError:
    print("ERROR: please define BASEPATH in the config file")
    quit()

if not os.path.isdir(BASEPATH):
    print(
        f"ERROR: the configured BASEPATH ({BASEPATH}) doesn't seem to exist.  Please check your config file"
    )
    quit()

try:
    BACKUPPATH = config["paths"]["backups"]
except KeyError:
    BACKUPPATH = f"{BASEPATH}/backups"
DBFPATH = f"{BASEPATH}/quotas.db"
DBURI = "file:{}".format(pathname2url(DBFPATH))


def upgradedb(dbver, quotrver, cur, con):
    backuppath = backupdb()
    dbver = Version(dbver)

    newver = Version("0.3.0")
    if dbver == "0.1.0":
        cur.execute("CREATE TABLE version(version)")
        cur.execute("INSERT INTO version VALUES ('0.3.0')")
        dbver = newver

    newver = Version("0.4.0")
    if dbver < newver:
        cur.execute("ALTER TABLE quotas ADD COLUMN type")
        dbver = newver

    newver = Version("0.8.0")
    if dbver < newver:
        cur.execute("ALTER TABLE quotas ADD COLUMN creation_date")
        cur.execute("ALTER TABLE quotas ADD COLUMN end_date")
        cur.execute("CREATE TABLE descriptions(quotas_id, description)")
        dbver = newver

    newver = Version("0.10.0")
    if dbver < newver:
        cur.execute("CREATE TABLE quotas_tmp AS SELECT * FROM quotas;")
        cur.execute("DROP table quotas;")
        cur.execute("CREATE TABLE quotas AS SELECT * FROM quotas_tmp;")
        cur.execute("DROP table quotas_tmp;")
        cur.execute(
            "CREATE UNIQUE INDEX unique_project_group ON quotas(project_name, req_group);"
        )
        dbver = newver

    newver = Version("1.1.0")
    if dbver < newver:
        cur.execute("ALTER TABLE quotas DROP COLUMN quota;")
        cur.execute("ALTER TABLE quotas DROP COLUMN quotal;")
        cur.execute("ALTER TABLE quotas DROP COLUMN iquota;")
        cur.execute("ALTER TABLE quotas DROP COLUMN iquotal;")
        dbver = newver

    cur.execute("UPDATE version SET version = ?", (quotrver,))
    con.commit()
    print("Upgrade complete.")
    print(f"Previous DB backup can be found at {backuppath}.")


def generate_cmds(
    config,
    quotas_id,
    req_user,
    req_group,
    quota,
    iquota,
    qtype,
    path,
    mkdir=True,
):
    quotal = quota
    iquotal = iquota

    cmds = []
    cmd_templates = []

    if mkdir:
        try:
            cmd_templates += config["commands"]["mkdir"][qtype]
        except KeyError:
            raise ValueError(f"Could not get commands for quota type '{qtype}'")

    cmd_templates.append(config["commands"]["setquota"])

    for c in cmd_templates:
        cmds.append(
            c.format(
                pid=quotas_id,
                req_user=req_user,
                req_group=req_group,
                quota=int(quota / 1000),
                iquota=iquota,
                path=path,
            )
        )

    return cmds


def checkdb():
    con, cur = connect()
    try:
        cur.execute("SELECT version from version")
        dbver = cur.fetchone()[0]
    except sqlite3.OperationalError:
        dbver = "0.1.0"
    finally:
        if Version(dbver) < Version(VERSION):
            if click.confirm("New quotr version detected. Perform database upgrade?"):
                upgradedb(dbver, VERSION, cur, con)
            quit()
        elif Version(dbver) > Version(VERSION):
            raise sqlite3.Warning(
                f"DB version ({dbver}) is newer than the installed quotr version ({VERSION}). Please upgrade quotr."
            )

        con.close()


@click.command()
@click.option("--id", "pid", type=int, help="ID of the quota entry")
@click.option(
    "--project_name", "-p", help="name of the project associated with the quota"
)
@click.option("--req_user", "-u", help="requesting user")
@click.option("--req_group", "-g", help="group the quota belongs to")
@click.option(
    "--end_from",
    help="end date from...",
    type=click.DateTime(input_date_formats),
)
@click.option(
    "--end_to",
    help="end date to...",
    type=click.DateTime(input_date_formats),
)
@click.option(
    "--type",
    "-t",
    "qtype",
    type=click.Choice(["project", "user"], case_sensitive=False),
    help="quota type",
)
@click.option(
    "--cmds",
    "-c",
    is_flag=True,
    help="print commands to set the quotas in the file system",
)
@click.option(
    "--delete",
    is_flag=True,
    help="mark resulting quotas as deleted",
)
@click.option(
    "--restore",
    is_flag=True,
    help="restore deleted quotas",
)
@click.option(
    "--limit",
    "-l",
    help="max number of rows to display",
    default=config["row_limit"],
    show_default=True,
)
@click.option(
    "--long",
    is_flag=True,
    help="show more columns",
)
@click.option(
    "--json",
    is_flag=True,
    help="output data in json format",
)
def lst(
    pid,
    project_name,
    req_user,
    req_group,
    end_from,
    end_to,
    qtype,
    cmds,
    delete,
    restore,
    limit,
    long,
    json,
):
    if delete and restore:  # ha ha ha
        delete = False
        restore = False

    if limit < 0:
        limit = None

    # If the user is not an admin and a) they don't specify a group or b) don't belong to the group 
    # they specified, then limit the output to their user ID
    if not isadmin():
        if not req_group or not req_group in set([grp.getgrgid(g).gr_name for g in os.getgroups()]):
            req_user = getpass.getuser()

    db_fields = [
        "project_name",
        "req_user",
        "req_group",
        "end_date",
        "type",
    ]

    fs_fields = ["usage", "quota", "files", "limit"]

    if long:
        db_fields.append("deleted")

    t = PrettyTable()
    fn = ["id"] + db_fields + fs_fields
    if long:
        fn.append("paths")
    t.field_names = fn

    query = "SELECT rowid, " + ", ".join(db_fields) + " FROM quotas"
    where = " WHERE 1=1"
    params = []

    if pid:
        where += f" AND rowid = ?"
        params.append(pid)
    if project_name:
        where += f" AND project_name = ?"
        params.append(project_name)
    if req_user:
        where += f" AND req_user = ?"
        params.append(req_user)
    if req_group:
        where += f" AND req_group = ?"
        params.append(req_group)
    if end_from:
        where += f" AND end_date >= ?"
        params.append(end_from)
    if end_to:
        where += f" AND end_date <= ?"
        params.append(end_to)
    if qtype:
        where += f" AND type = ?"
        params.append(qtype)

    query += where

    query += " ORDER BY rowid DESC"

    if limit:
        query += f" LIMIT {limit}"

    commands = []

    con, curq = connect()
    curp = con.cursor()

    for rowindex, rq in enumerate(curq.execute(query, params)):
        row = dict(rq)

        try:
            row["end_date"] = row["end_date"].split()[0]
        except AttributeError:
            pass

        quotas_id = row["rowid"]
        req_user = row["req_user"]
        req_group = row["req_group"]
        qtype = row["type"]

        paths = []
        for rp in curp.execute(f"SELECT * FROM paths WHERE quotas_id = {quotas_id}"):
            paths.append(rp[1])

        (
            kbytes,
            quota,
            quotal,
            grace,
            nfiles,
            iquota,
            iquotal,
            igrace,
        ) = getfsquota(quotas_id, config["paths"]["storage"])

        try:
            quotapct = f"{kbytes/quota:.0%}"
        except ZeroDivisionError:
            quotapct = "-"

        try:
            nfilespct = f"{nfiles/iquotal:.0%}"
        except ZeroDivisionError:
            nfilespct = "-"

        trow = list(row.values()) + [
            f"{format_size(kbytes)} ({quotapct})",
            format_size(quotal),
            f"{nfiles} ({nfilespct})",
            iquotal,
        ]

        if long:
            trow.append(",".join(paths))

        t.add_row(trow)
        if cmds:
            commands += generate_cmds(
                config,
                quotas_id,
                req_user,
                req_group,
                quota,
                iquota,
                qtype,
                paths[0],  # should be changed if we ever implement multiple paths
            )
    if cmds:
        for c in commands:
            print(c)
    else:
        if json:
            print(t.get_json_string())
        else:
            print(t)

    try:
        if limit == rowindex + 1:
            print(
                f"\nShowing the {limit} most recent entries. Use `-l` to change limit.\n"
            )
    except UnboundLocalError:
        pass

    if restore or delete:
        if restore:
            value = 0
        elif delete:
            value = 1
        else:
            raise ValueError("Wrong value for delete or restore.")

        if click.confirm("Mark these quotas as deleted?"):
            curq.execute(f"UPDATE quotas SET deleted = {value}" + where, params)
            con.commit()

    con.close()


@click.command()
@click.option("--project_name", required=True, help="name of the project")
@click.option("--req_user", required=True, help="requesting user")
@click.option("--req_group", required=True, help="group the quota belongs to")
@click.option(
    "--quota",
    required=True,
    help="quota soft limit",
    default="1T",
    show_default=True,
)
@click.option(
    "--iquota",
    required=True,
    help="inode quota soft limit",
    default=100000,
    show_default=True,
)
@click.option(
    "--end_date",
    required=False,
    help="project end date",
    type=click.DateTime(input_date_formats),
)
@click.option(
    "--type",
    "qtype",
    type=click.Choice(["project", "user"], case_sensitive=False),
    default="project",
    show_default=True,
    help="quota type",
    required=True,
)
@click.option("--path", required=True, help="path to the project directory")
@click.option("--description", help="project description")
@click.option(
    "--dry-run",
    "-d",
    is_flag=True,
    help="print commands but don't actually execute anything",
)
def add(
    project_name,
    req_user,
    req_group,
    quota,
    iquota,
    end_date,
    qtype,
    path,
    description,
    dry_run,
):
    checkperm()

    quota = parse_size(quota)
    iquota = int(iquota)

    if dry_run:
        i = "?"
    else:
        if safe:
            backupdb()

        con, cur = connect()
        cur.execute(
            f"INSERT INTO quotas (project_name,  req_user, req_group, creation_date, end_date, type, deleted) VALUES (?, ?, ?, ?, ?, ?, FALSE)",
            (
                project_name,
                req_user,
                req_group,
                datetime.date.today(),
                end_date,
                qtype,
            ),
        )
        i = cur.lastrowid
        cur.execute(f"INSERT INTO paths VALUES (?, ?)", (i, path.rstrip("/")))
        cur.execute(f"INSERT INTO descriptions VALUES (?, ?)", (i, description))
        con.commit()
        con.close()

    for c in generate_cmds(
        config,
        i,
        req_user,
        req_group,
        quota,
        iquota,
        qtype,
        path,
    ):
        if dry_run:
            print(c)
        else:
            try:
                subprocess.run(shlex.split(c), check=True)
            except FileNotFoundError:
                print("ERROR: lfs not found in the system. Unable to create quota.")
            else:
                print("Command successful.")


@click.command()
@click.option("--id", required=True, type=int, help="ID of the quota entry to update")
@click.option("--project_name", help="name of the project associated with the quota")
@click.option("--req_user", help="requesting user")
@click.option("--req_group", help="group the quota belongs to")
@click.option("--quota", help="quota soft limit")
@click.option("--iquota", help="inode quota soft limit")
@click.option(
    "--end_date",
    help="project end date",
    type=click.DateTime(input_date_formats),
)
@click.option(
    "--type",
    "qtype",
    type=click.Choice(["project", "user"], case_sensitive=False),
    help="quota type",
)
@click.option("--path", help="path to the project directory")
@click.option("--description", help="project description")
@click.option(
    "--dry-run",
    "-d",
    is_flag=True,
    help="print commands but don't actually execute anything",
)
def update(
    id,
    project_name,
    req_user,
    req_group,
    quota,
    iquota,
    end_date,
    qtype,
    path,
    description,
    dry_run,
):
    checkperm()

    quotas_id = id

    if quota:
        quota = parse_size(quota)
    if iquota:
        iquota = int(iquota)

    con, cur = connect()

    cur.execute(
        "SELECT * FROM quotas q, paths p WHERE q.rowid = ? AND q.rowid = p.quotas_id",
        (quotas_id,),
    )
    (
        old_project_name,
        old_req_user,
        old_req_group,
        old_qtype,
        old_deleted,
        old_creation_date,
        old_end_date,
        old_quotas_id,
        old_path,
    ) = cur.fetchone()

    (
        old_kbytes,
        old_quota,
        old_quotal,
        old_grace,
        old_nfiles,
        old_iquota,
        old_iquotal,
        old_igrace,
    ) = getfsquota(quotas_id, config["paths"]["storage"])

    quota = quota or old_quota
    iquota = iquota or old_iquota
    path = config["paths"]["storage"]

    if not dry_run:
        if safe:
            backupdb()
        cur.execute(
            """
                UPDATE quotas SET
                    project_name = COALESCE(?, project_name),
                    req_user = COALESCE(?, req_user),
                    req_group = COALESCE(?, req_group),
                    end_date = COALESCE(?, end_Date),
                    type = COALESCE(?, type)
                WHERE rowid = ?
            """,
            (
                project_name,
                req_user,
                req_group,
                end_date,
                qtype,
                quotas_id,
            ),
        )

        if path:
            cur.execute(
                "UPDATE paths SET path = COALESCE(?, path) WHERE quotas_id = ?",
                (path.rstrip("/"), quotas_id),
            )
        if description:
            cur.execute(
                "UPDATE descriptions SET description = COALESCE(?, description) WHERE quotas_id = ?",
                (description, quotas_id),
            )
            cur.execute(
                "INSERT OR IGNORE INTO descriptions (quotas_id, description) VALUES (?, ?)",
                (quotas_id, description),
            )
        con.commit()

    for c in generate_cmds(
        config,
        quotas_id,
        req_user,
        req_group,
        quota,
        iquota,
        qtype,
        path,
        mkdir=False,
    ):
        if dry_run:
            print(c)
        else:
            try:
                subprocess.run(shlex.split(c), check=True)
            except FileNotFoundError:
                print("ERROR: lfs not found in the system. Unable to update quota.")
            else:
                print("Quota updated successfully.")

    con.close()


@click.group()
def quota():
    pass


quota.add_command(lst, name="list")
quota.add_command(add)
quota.add_command(update)


@click.command()
@click.option(
    "--overwrite",
    is_flag=True,
    show_default=True,
    default=False,
    help="overwrite the database",
)
def initdb(overwrite):
    try:
        con = sqlite3.connect(DBURI, uri=True)
    except:
        createdb()
        print("Database initialised")
    else:
        if overwrite:
            backupdb(delete=True)
            createdb()
            print("Database reinitialised")
        else:
            print("Database exists. Use --overwrite to replace it with an empty one.")


@click.command()
@click.option(
    "--send-email",
    "-m",
    is_flag=True,
    help="send notification email",
)
def checkexp(send_email):
    con, cur = connect()
    today = datetime.date.today()
    m = ""
    for rq in cur.execute("SELECT rowid, * from quotas where end_date < ?", (today,)):
        m += f'Project {rq[0]} "{rq[1]}" (user: {rq[2]}, group: {rq[3]})\n'

    if m:
        print(m)
        if send_email:
            sendmail(
                config["mail"]["subject"],
                config["mail"]["from"],
                config["mail"]["to"],
                m,
            )
            print("Email notification sent")
    else:
        print("There are currently no expired projects")


@click.group()
@click.version_option()
@click.pass_context
def cli(ctx):
    if ctx.invoked_subcommand != "initdb":
        checkdb()
    pass


cli.add_command(quota)
if isadmin():
    cli.add_command(initdb)
    cli.add_command(checkexp)

if __name__ == "__main__":
    cli()
